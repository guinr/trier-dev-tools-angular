import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scrum',
  templateUrl: './scrum.component.html',
  styleUrls: ['./scrum.component.css']
})
export class ScrumComponent implements OnInit {

  cards = [
    { number: '0' },
    { number: '1/2' },
    { number: '1' },
    { number: '2' },
    { number: '3' },
    { number: '5' },
    { number: '8' },
    { number: '13' },
    { number: '20' },
    { number: '40' },
    { number: '100' },
    { number: '?' },
  ]

  showCards: boolean;
  selectedCard: any;

  constructor() { }

  ngOnInit() {
    this.showCards = true;
  }
  
  selectCard(card: any) {
    this.selectedCard = card;
    this.showCards = false;
  }

  backToCards() {
    this.showCards = true;
  }

}
