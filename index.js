var express = require('express');
var app = express();

app.use(express.static(__dirname + '/dist/trier-dev-tools'));

app.get('*', function(req, res) {
    res.sendFile(__dirname + '/dist/trier-dev-tools/index.html');
});

app.listen(process.env.PORT || 3000);